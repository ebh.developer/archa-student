import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class StudentData {
  final int id;
  final String firstName;
  final String lastName;
  final String course;
  final int score;
  final String createAt;
  final String updateAt;

  StudentData(
      {required this.id,
      required this.firstName,
      required this.lastName,
      required this.course,
      required this.score,
      required this.createAt,
      required this.updateAt});

  StudentData.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        firstName = json['first_name'],
        lastName = json['last_name'],
        course = json['course'],
        score = json['score'],
        createAt = json['created_at'],
        updateAt = json['updated_at'];
}

class HttpClient {
  static Dio instance =
      Dio(BaseOptions(baseUrl: 'http://expertdevelopers.ir/api/v1/'));
}

Future<List<StudentData>> getStudents() async {
  final response = await HttpClient.instance.get('experts/student');
  print('studentList ->:${response.data}');
  final List<StudentData> students = [];
  if (response.data is List<dynamic>) {
    (response.data as List<dynamic>).forEach((element) {
      students.add(StudentData.fromJson(element));
    });
  }

  return students;
}

Future<StudentData> saveStudent(
    String firstName, String lastName, String course, int scoure) async {
  final response = await HttpClient.instance.post('experts/student', data: {
    'first_name': firstName,
    'last_name': lastName,
    'course': course,
    'score': scoure,
  });
  if (response.statusCode == 200) {
    return StudentData.fromJson(response.data);
  } else {
    throw Exception();
  }
}
